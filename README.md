# Craigslist Crawler
This script is a crawler which extract all information about advertisements of [Craiglist](https://craigslist.org) and save in local storage or in mongoDB.

## This project is supposed to gather below information
- title
- price
- description
- post id
- created time
- modified time
- images

## Requirements
- Python 3.6+
- MongoDB 4.4

## Quick Start
- Create a directory and name it "Craigslist Crawler"
- Open a terminal in this directory
- Create a virtualenv:

    ```shell script
    virtualenv -p python3 Venv
    ```
- Activate virtualenv:

    ```shell script
    sourse Venv/bin/activate
    ```

- Clone this project:

    ```shell script
    git clone https://gitlab.com/mr_nasiri/craigslist_crawler.git src
    ```
- Change directory to "src":

    ```shell script
    cd src
    ```
- Install require packages:

    ```shell script
    pip install -r requirements.txt
    ```

- Install mongoDB and start it

- choose and change Storage_type in config

- Run main.py and enjoy...

    - Run main.py with 'find_links' parameter to find advertisements links:

        ```shell script
        python main.py find_links
        ```
    - Run main.py with 'extract_pages' parameter to extract information of advertisements:

        ```shell script
        python main.py extract_pages
        ```
    - Run main.py with 'download_images' parameter to download images of advertisements:

        ```shell script
        python main.py download_images
        ```
## Bulit with:
- [Requests](https://pypi.org/project/requests/) -  Used for make HTTP requests to get pages
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) - Used for parse and extract information of pages
- [Pymongo](https://pymongo.readthedocs.io/en/stable/tutorial.html) - Used for save information to mongo DataBase        

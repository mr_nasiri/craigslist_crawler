import json
import os

from abc import ABC, abstractmethod
from mongo import MongoDatabase


class StorageAbstract(ABC):

    @abstractmethod
    def store(self, data, *args):
        pass

    @abstractmethod
    def load(self, *args, **kwargs):
        pass

    @abstractmethod
    def update_flag(self, data):
        """ control crawl links for only one time with flags"""
        pass


class MongoStorage(StorageAbstract):

    def __init__(self):
        self.mongo = MongoDatabase()

    def store(self, data, collection, *args):
        collection = getattr(self.mongo.database, collection)
        if isinstance(data, list) and len(data) > 1:
            collection.insert_many(data)
        else:
            collection.insert_one(data)
            print(data['post_id'])

    def load(self, collection_name, filter_data=None):
        collection = self.mongo.database.get_collection(collection_name)
        if filter_data is not None:
            data = collection.find(filter_data)
        else:
            data = collection.find()
        return data

    def update_flag(self, data):
        self.mongo.database.advertisement_links.find_one_and_update(
            {'_id': data['_id']},
            {'$set': {'flag': True}}
        )


class FileStorage(StorageAbstract):

    def store(self, data, filename, *args):
        if filename == 'advertisement_data':
            path = f"fixtures/adv/{filename + '-' + data['post_id']}.json"
        else:
            path = f"fixtures/{filename}.json"

        with open(path, 'w') as f:
            f.write(json.dumps(data))
        print(path)

    def load(self, filename, filter_data=None):

        if filename == "advertisement_links":
            with open(f'fixtures/{filename}.json', 'r') as f:
                links = json.loads(f.read())
            return [link for link in links if not link['flag']]

        else:
            advertisements = list()
            path = "fixtures/adv/"
            for adv in os.listdir(path):
                with open(path+adv, 'r') as f:
                    advertisements.append(json.loads(f.read()))
            return advertisements

    def update_flag(self, data):
        with open("fixtures/advertisement_links.json", 'r') as f:
            links = json.loads(f.read())

        for link in links:
            if link['url'] == data['url']:
                link['flag'] = True
                break

        with open("fixtures/advertisement_links.json", 'w') as f:
            f.write(json.dumps(links))

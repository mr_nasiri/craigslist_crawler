import os

import requests

from abc import ABC, abstractmethod
from bs4 import BeautifulSoup
from config import BASE_LINK, STORAGE_TYPE
from parser import AdvertisementPageParser
from storage import MongoStorage, FileStorage


class CrawlerBase(ABC):

    def __init__(self):
        self.storage = self.__set_storage()

    @staticmethod
    def __set_storage():
        if STORAGE_TYPE == 'mongo':
            return MongoStorage()
        return FileStorage()

    @abstractmethod
    def start(self, store=False):
        pass

    @abstractmethod
    def store(self, data, filename=None):
        pass

    @staticmethod
    def get(link):
        try:
            page_response = requests.get(link)
        except requests.HTTPError:
            return None
        return page_response


class LinkCrawler(CrawlerBase):

    def __init__(self, cities, link=BASE_LINK):
        self.cities = cities
        self.link = link
        super().__init__()

    @staticmethod
    def find_links(html_doc):
        soup = BeautifulSoup(html_doc, 'html.parser')
        return soup.find_all('a', attrs={'class': 'hdrlnk'})

    def start_crawl_city(self, url):
        start = 0
        crawl = True
        adv_links = list()
        while crawl:
            response = self.get(url+str(start))
            new_links = self.find_links(response.text)
            adv_links.extend(new_links)
            start += 120
            crawl = bool(len(new_links))

        return adv_links

    @staticmethod
    def create_directory():
        if not os.path.exists("fixtures") and STORAGE_TYPE == 'file':
            os.mkdir("fixtures")

    def start(self, store=False):
        self.create_directory()
        adv_links = list()
        for city in self.cities:
            links = self.start_crawl_city(self.link.format(city))
            print(f"{city} total: {len(links)}")
            adv_links.extend(links)
        if store:
            self.store([{"url": li.get('href'), "flag": False} for li in adv_links])

        return adv_links

    def store(self, data, *args):
        self.storage.store(data, 'advertisement_links')


class DataCrawler(CrawlerBase):

    def __init__(self):
        super().__init__()
        self.links = self.__load_links()
        self.parser = AdvertisementPageParser()

    def __load_links(self):
        return self.storage.load('advertisement_links', {'flag': False})

    @staticmethod
    def create_directory():
        if not os.path.exists("fixtures/adv") and STORAGE_TYPE == 'file':
            os.mkdir("fixtures/adv")

    def start(self, store=False):
        self.create_directory()
        for link in self.links:
            response = self.get(link['url'])
            data = self.parser.parse(response.text)
            if store:
                self.store(data, data.get('post_id', 'sample'))

            self.storage.update_flag(link)

    def store(self, data, filename=None):
        self.storage.store(data, 'advertisement_data')


class ImageDownloader(CrawlerBase):

    def __init__(self):
        super().__init__()
        self.advertisements = self.__load_advertisements()
    
    def __load_advertisements(self):
        return self.storage.load('advertisement_data')

    @staticmethod
    def get(link):
        try:
            page_response = requests.get(link, stream=True)
        except requests.HTTPError:
            return None
        return page_response

    @staticmethod
    def create_directory():
        if not os.path.exists("fixtures/images") and STORAGE_TYPE == 'file':
            os.mkdir("fixtures/images")

    def start(self, store=True):
        self.create_directory()
        for advertisement in self.advertisements:
            counter = 1
            for image in advertisement['images']:
                response = self.get(image['url'])
                if store:
                    self.store(response, advertisement['post_id'], counter)
                counter += 1
            print(f"advertisement {advertisement['post_id']} ({counter-1}) images saved.")

    def store(self, data, adv_id, img_number):
        if not os.path.exists(f"fixtures/images/{adv_id}"):
            os.mkdir(f"fixtures/images/{adv_id}")

        with open(f'fixtures/images/{adv_id}/{img_number}.jpg', 'wb') as f:
            for chunk in data.iter_content(chunk_size=1024):
                f.write(chunk)
